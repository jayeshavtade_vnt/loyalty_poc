
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AddEarningRule</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="jquery-1.7.1.min.js"></script>
</head>
<body>
  	<div class="div_body" id="div_body">
  		<form action="addearningrule_action.php" method="post" enctype="multipart/form-data">
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<h3>Basic Information</h3>
	  			<label>Name</label>
	  			<input type="text" name="name" id="name" required><br>
	  			<label>Description</label>
	  			<textarea name="description" required></textarea><br>
	  			<label>Active</label>
	  			<select name="active">
	  				<option value="0" selected="selected">Inactive</option>
	  				<option value="1">Active</option>
	  			</select>
	  			<br>
	  			<br>
	  		</div>
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<h3>Type Details</h3>
	  			<label>Type *</label>
	  			<select name="type_name" id="type_name" required>
            <option value="" style="display:none;"></option>
	  				<option value="Customer Referral">Customer Referral</option>
	  			</select><br>
	  			<div id="type_details">
	  				
	  			</div>
	  			<br>
	  		</div>
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<h3>Activity of rule</h3>
	  			<label>All time active</label>
	  			<input type='checkbox' name='alltimeactive' value='1' id='alltimeactive'><br>
	  			<div id="activity_period">
		  			<label>Start at *</label>
		  			<input type="date" name="startat" required>
		  			<br>
		  			<label>End at *</label>
		  			<input type="date" name="endat" required>
	  			</div>
	  			<br>
	  		</div>
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<h3>Target</h3>
	  			<label>Target type *</label>
	  			<select name="target_type_name" id="target_type_name">
	  				<option value="0">Level</option>
	  				<option value="1">Segment</option>
	  			</select><br>
	  			<div id="target_type_div">
		  			<label>Levels *</label>
            <div id="target_type_selector_div" style="display: inline-block;">
              <select name="target_type_items" id="target_type_items">
              <!-- <option value="" style="display:none;"></option> -->
              <!-- <option value="0">Gold</option>
              <option value="1">Silver</option>
              <option value="2">Brownze</option> -->
              </select>
            </div>
            <div id="target_type_selected_list">
              <div class="target_type_selected_list_element"><span>gold</span><span class="target_type_selected_list_element_clear" onclick="remove_target_type('gold',this);">X</span>
              <input type="hidden" name="target_type_selected_list_element[]" value="gold"></div>
              <!-- <div class="target_type_selected_list_element"><span>Gold</span><span class="target_type_selected_list_element_clear">X</span></div>
              <div class="target_type_selected_list_element"><span>Gold</span><span class="target_type_selected_list_element_clear">X</span></div> -->
            </div>
            <br>
	  			</div>
	  			<br>
	  		</div>
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<h3>Earning rule photo</h3>
	  			<label>Earning rule photo</label>
	  			<input type="file" name="earningrulephoto" id="earningrulephoto">
          <img id="earningrulephoto_view" src="" width="150">
          <span id="remove_img" hidden="hidden">X</span>
	  			<br>
	  			<br>
	  		</div>
	  		<div class="row" style="border:1px solid black; margin: 5px; padding: 5px;">
	  			<input type="submit" value="Save" name="submit">
	  			<input type="button" value="Cancel">
	  			<br>
	  		</div>
	  	</form>
    </div>
<style type="text/css">
  .target_type_selected_list_element_clear{
    display: inline-block;
    background-color: red;
    width: auto;
    color: white;
    padding: 0 5px;
    margin: 0 5px;
    align-items: center;
    border-radius: 3px;
  }
  .target_type_selected_list_element {
    display: inline-block;
    background-color: deepskyblue;
    width: auto;
    padding: 5px;
    border-radius: 5px;
    margin: 0 5px;
    border: 1px solid deepskyblue;
}
</style>
  
  <script type="text/javascript">
  	$(document).ready(function(){
      fill_target_type("Level");
      /*var target_type_selector_div_data = "<select name='target_type_items' id='target_type_items'>"+
                "<option value='' style='display:none;'></option>"+
                "<option value='0'>Gold</option>"+
                "<option value='1'>Silver</option>"+
                "<option value='2'>Brownze</option>"+
                "</select>";
      $('#target_type_selector_div').html(target_type_selector_div_data);
*/
  		$('#type_name').on('change', function() {
  			var type_details_body = "";
  			if($('#type_name').val() == "Customer Referral")
  			{
				type_details_body = "<label>Custom Event name *</label>"+
		  			"<input type='text' name='type_name_data[]' id='customeventname'><br>"+
            "<input type='hidden' name='type_name_name[]' value='customeventname'><br>"+
		  			"<label>Points *</label>"+
		  			"<input type='text' name='type_name_data[]' id='points'><br>"+
            "<input type='hidden' name='type_name_name[]' value='points'><br>"+
		  			"<label>Usage limit active</label>"+
		  			"<input type='checkbox' name='type_name_data[]' value='1' id='usagelimitactive'><br>"+
            "<input type='hidden' name='type_name_name[]' value='usagelimitactive'><br>";
  			}
  			else if($('#type_name').val() == "2")
  			{
  				type_details_body = "";
  			}
  			$('#type_details').html(type_details_body);
		  });	
  		$('#alltimeactive').on('click', function() {
  			var activity_period_body = "";
    			if($('#alltimeactive')[0].checked === true)
    			{
    				activity_period_body = "";
    			}
    			else
    			{
    				var activity_period_body = "<label>Start at *</label>"+
  		  			"<input type='date' name='startat' required>"+
  		  			"<br>"+
  		  			"<label>End at *</label>"+
  		  			"<input type='date' name='endat' required>";
    			}
    			$('#activity_period').html(activity_period_body);
  		});
  		$('#target_type_name').on('change', function() {

          fill_target_type($('#target_type_name').val());
          //$('#target_type_selector_div').html(target_type_selector_div_data);
  		});
      jQuery(document).on("change","#target_type_items",function(){
        var title_value = $(this).val();
        var title_name = $("#target_type_items option[value=" + title_value + "]").html();
        //alert(title);

        var target_type_items_items = '<div class="target_type_selected_list_element"><span>'+
          ''+title_name+'</span><span class="target_type_selected_list_element_clear"'+
           'onclick="remove_target_type('+title_value+',this);">X</span>'+
           '<input type="hidden" name="target_type_selected_list_element[]" value="'+title_name+'"></div>';
        
        $('#target_type_selected_list').append(target_type_items_items);
        $("#target_type_items option[value=" + title_value + "]").hide();
        $("#target_type_items").val("");
      });
      $("#earningrulephoto").change(function(){
          readURL(this);
      });
      $('#remove_img').on('click', function() {
          $("#earningrulephoto").val("");
          $('#earningrulephoto_view').removeAttr("src");
          $('#remove_img').hide();
      });
  	});

    var globalrowno = 0;
    function fill_target_type(target_type_name)
    {
      var target_type_name = "";
      var target_type_selector_div_data = "";
      $('#target_type_selected_list').html("");
        if(target_type_name == "0")
        {
          $('#target_type_div label').html("Levels *");
          /*target_type_selector_div_data = "<select name='target_type_items' id='target_type_items'>"+
              "<option value='' style='display:none;'></option>"+
              "<option value='0'>Gold</option>"+
              "<option value='1'>Silver</option>"+
              "<option value='2'>Brownze</option>"+
              "</select>";*/
              target_type_name = "Level";
        }
        else if(target_type_name == "1")
        {
          $('#target_type_div label').html("Segments *");
              target_type_name = "Segment";
        }
        $.ajax({
          url:"ajax_get_target_type_elemnts.php",
          type:"POST",
          data:{target_type:target_type_name},
          dataType:"text",
          success:function(data)
          {
            var obj=JSON.parse(data);
            $("#target_type_items option").remove();
            $("#target_type_items").append($("<option style='display:none;'></option>").val("").html(""));
            $.each(obj,function(key,val) {
              $("#target_type_items").append(
                $("<option></option>").val(key).html(val.name)
              );     
            });
          }
        });
    }
    function remove_target_type(target,me){
      //alert(target);
      $("#target_type_items option[value=" + target + "]").show();
      $(me).closest(".target_type_selected_list_element").remove();
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();       
            reader.onload = function (e) {
                $('#earningrulephoto_view').attr('src', e.target.result);
                $('#remove_img').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function editview(header,rowno) {
      globalrowno = rowno;
      $("#headertext").html(header);
      $('#update').html('EDIT');
      $('#logintable').find("tr:eq(1)").show();
      $('#logintable').find("tr:eq(7)").hide();
      $(".pageeditview").css({"display":"block"});
      $('#edituserid').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(0)").html());
      $('#editusername').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(1)").html());
      $('#editusercontact').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(2)").html());
      $('#editmail_id').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(3)").html());
      $('#editsubject').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(4)").html());
      $('#editmessage').val($('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(5)").html());
    }

    function updateedit() {

      var id= $('#edituserid').val();
      var name= $('#editusername').val();
      var contact= $('#editusercontact').val();
      var mail_id= $('#editmail_id').val();
      var subject= $('#editsubject').val();
      var message= $('#editmessage').val();
      var password= $('#editpassword').val();
      if($("#update").html() == "EDIT"){

        if(id != '')
        {
          $.ajax({
            url:"updateuserdetails.php",
            type:"POST",
            data:{id:id,name:name,contact:contact,mail_id:mail_id,subject:subject,message:message},
            dataType:"JSON",
            success:function(data)
            {
              if(data != "Cannot Update record try again....."){
                $('#userlisttable').find("tr:eq("+globalrowno+")").find("td:eq(1)").html(data.name);
                $('#userlisttable').find("tr:eq("+globalrowno+")").find("td:eq(2)").html(data.contact);
                $('#userlisttable').find("tr:eq("+globalrowno+")").find("td:eq(3)").html(data.email);
                $('#userlisttable').find("tr:eq("+globalrowno+")").find("td:eq(4)").html(data.subject);
                $('#userlisttable').find("tr:eq("+globalrowno+")").find("td:eq(5)").html(data.message);
                $(".pageeditview").css({"display":"none"});
              }
            }
          });  
        }
      }
      else{

        if(name != '')
        {
          $.ajax({
            url:"insertnewuser.php",
            type:"POST",
            data:{name:name,contact:contact,mail_id:mail_id,subject:subject,message:message,password:password},
            dataType:"JSON",
            success:function(data)
            {
              if(data != "Cannot Add User try again....."){
                $('#userlisttable').append('<tr><td>'+ data.id +'</td><td>'+ data.name +'</td><td>'+ data.contact +'</td><td>'+ data.email +'</td><td>'+ data.subject +'</td><td>'+ data.message +'</td><td></td></tr>');

                var rowCount = $('#userlisttable tr').length;
                $('<button></button>').attr({'class': 'buttonclass editbuttonclass','onclick': 'editview("EDIT USER DETAILS","'+(rowCount-1) +'")'}).appendTo($('#userlisttable').find("tr:eq("+(rowCount-1)+")").find("td:eq(6)"));

                $('<button></button>').attr({'class': 'buttonclass deletebuttonclass','onclick': 'deleteuser("'+(rowCount-1) +'")'}).appendTo($('#userlisttable').find("tr:eq("+(rowCount-1)+")").find("td:eq(6)"));
                $('#userlisttable tr:nth-child('+(rowCount-1)+')').show();
                $(".pageeditview").css({"display":"none"});
              }
            }
          }); 
        }
      }
    }

    function canceledit() {
     $(".pageeditview").css({"display":"none"});
   }

   function deleteuser(rowno) {
     var id = $('#userlisttable').find("tr:eq("+rowno+")").find("td:eq(0)").html();
     if(id != '')
     {
      $.post(
        "deleteuser.php",
        {id:id},
        function(result){
          alert(result);
          $('#userlisttable').find("tr:eq("+rowno+")").remove();
        },);
    }
  }
  function Adduserfun(header){

    $('#edituserid').val('');
    $('#editusername').val('');
    $('#editusercontact').val('');
    $('#editmail_id').val('');
    $('#editsubject').val('');
    $('#editmessage').val('');
    $('#update').html('ADD');
    $("#headertext").html(header);
    $('#logintable').find("tr:eq(1)").hide();
    $('#logintable').find("tr:eq(7)").show();
    $(".pageeditview").css({"display":"block"});
  }

</script>
</body>
</html>